/* ---------------------------------------
 Exported Module Variable: JSONEditor4Code
 Package:  jsoneditor4code
 Version:  0.9.1  Date: 2018/10/04 13:03:19
 Homepage: https://niebert.github.io/ClassEditorUML
 Author:   Engelbert Niehaus
 License:  MIT
 Require Module with:
    const JSONEditor4Code = require('jsoneditor4code');
    var  compileCode = JSONEditor4Code.compile(vTemplate);
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */
