vDataJSON["uml_default"]={
    "data":{
      "classname": "MyClass",
      "superclassname": "",
      "comment": "",
      "reposinfo": {
          "repository": "",
          "require_classes": "yes",
          "static": "no",
          "author": "",
          "email": "",
          "created": getDateTime(),
          "modified": getDateTime(),
          "requirelist": []
      },
      "attributes": [],
      "methods": []
    },
    "settings":{
      "extension4code":".js",
      "localclasslist": [
          "LoadSaver",
          "LinkParam"
      ],
      "remoteclasslist": [
          "JSONEditor"
      ],
      "baseclasslist": [
          {
            "name": "Array",
            "initvalue": "[]"
          },
          {
              "name": "Boolean",
              "initvalue": "true"
          },
          {
              "name": "Float",
              "initvalue": "0.0"
          },
          {
              "name": "Function",
              "initvalue": "function my_fun() {}"
          },
          {
              "name": "Document",
              "initvalue": "document"
          },
          {
              "name": "Integer",
              "initvalue": "0"
          },
          {
              "name": "String",
              "initvalue": "\"\""
          },
          {
              "name": "Hash",
              "initvalue": "{}"
          },
          {
              "name": "Object",
              "initvalue": "null"
          },
          {
              "name": "RegularExp",
              "initvalue": "/search/g"
          }
      ]
    }
}
